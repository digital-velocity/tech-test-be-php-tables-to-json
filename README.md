#Tech Test - PHP Tables to JSON#

##Client Request:##
The client would like to add a store locator to their website, so that their customers can quickly find their local store.

The client currently only sells to customers within the UK, but only has stores within a 20-30 mile radius (West Yorkshire region).

Before we can add the store locator view into the website, we need to create a JSON feed to pull out the store data in the database.

The store locator on the frontend will need to pass in a number of different parameters, to refine their search for stores.

##Success criteria:##
- An endpoint that when hit will return an array of branch objects, ready for the view

---------------------------------

###Pointers/Information:###

We don’t require the task to be completed in full, as we know this is a task that is to be complete in the candidates own time.
 
However, we would ask that elements of the test that aren’t completed are described or documented as comments in the code. This gives us a greater incline as to where the candidate was looking to take the feature.

You will need to join both tables to return the correct data.

It would be good to see examples of use of a framework here.

Remember the application needs to be run on our environment, so please provide a small readme/install sheet so we have all the settings we require to launch the example.

---------------------------------

###Requirements:###

The end point should return an array of stores, based on the criteria passed in the query.

The ability to return:

- a limited set of results (return only 5 stores, for example).
- stores closest to a given location (lat/lng) in distance order.

---------------------------------


###Input Variables:###

Key | Description
------------ | -------------
**lat** | Customers current search latitude
**lng** | Customers current search longitude
**distance** | Radius in which to return search results for (returns all stores in x distance from search lat/lng)
**limit** | Count of stores to return in call

###Requested example response:###
```json
[  
	{  
		"branch_id" : "6",
		"branch" : "Wakefield Trinity Store",
		"slug" : "wakefield_trinity_store_wakefield",
		"distance" : 1.66,
		"address" : {  
			"address_1" : "Trinity shopping centre",
			"address_2" : "",
			"address_3" : "",
			"address_4" : "wakefield",
			"address_5" : "",
			"postcode" : "wf1 1qr"
		},
		"geo" : {  
			"latitude" : "53.68492",
			"longitude" : "-1.49661"
		},
		"profile" : {  
			"title" : "",
			"description" : "",
			"unit_type" : "store"
		},
		"contact" : {  
			"email" : "email@shop.com",
			"phone" : ""
		},
		"opening_times":{  
			"monday" : "09:00AM - 05:30PM",
			"tuesday" : "09:00AM - 05:30PM",
			"wednesday" : "09:00AM - 05:30PM",
			"thursday" : "09:00AM - 05:30PM",
			"friday" : "09:00AM - 05:30PM",
			"saturday" : "09:00AM - 05:30PM",
			"sunday" : "10:30AM - 04:00PM"
		},
		"links":{  
			"google_map_query" : "https:\/\/www.google.co.uk\/maps?q=53.68492,-1.49661"
		}
	}
]
```

---------------------------------

##Contacts:##
**David Ford** - [d.ford@digital-velocity.co.uk](d.ford@digital-velocity.co.uk)
**Richard Taylor-Jones** - [r.taylor-jones@digital-velocity.co.uk](r.taylor-jones@digital-velocity.co.uk)