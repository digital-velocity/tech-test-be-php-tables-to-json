DROP TABLE IF EXISTS `branch`;

CREATE TABLE `branch` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `store_num` double DEFAULT NULL,
  `branch_title` varchar(255) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `is_store_collection_available` tinyint(1) DEFAULT '0',
  `filename` varchar(255) DEFAULT NULL,
  `filename_2` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `business_page` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

LOCK TABLES `branch` WRITE;
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;

INSERT INTO `branch` (`id`, `store_num`, `branch_title`, `slug`, `is_active`, `is_store_collection_available`, `filename`, `filename_2`, `email`, `business_page`, `date_created`, `date_modified`)
VALUES
	(1,331,'St John\'s Store','st_john',1,0,'',NULL,NULL,NULL,NULL,NULL),
	(2,332,'White Rose Shop','white_rose',1,0,'',NULL,NULL,NULL,NULL,NULL),
	(3,357,'Kirkgate Shop','kirkgate',1,0,NULL,NULL,NULL,NULL,NULL,NULL),
	(6,481,'Wakefield Trinity Store','wakefield_trinity_store_wakefield',1,0,'',NULL,NULL,NULL,NULL,NULL),
	(7,252,'Shock Shop','shock_shop',1,0,NULL,NULL,NULL,NULL,NULL,NULL),
	(8,321,'Kirkgate Kiosk','kirkgate_kiosk_bradford',1,0,'',NULL,NULL,NULL,NULL,NULL),
	(9,346,'Feasegate Store','feasegate_store_york',1,0,'',NULL,NULL,NULL,NULL,NULL),
	(10,216,'Magazine World','magazine_world_vip_stockist_halifax',1,0,NULL,NULL,NULL,NULL,NULL,NULL),
	(11,223,'Northowram News','magazine_world',1,0,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `branch` ENABLE KEYS */;
UNLOCK TABLES;