# Dump of table branch_address
# ------------------------------------------------------------

DROP TABLE IF EXISTS `branch_address`;

CREATE TABLE `branch_address` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `store_id` int(2) NOT NULL,
  `add1` varchar(255) DEFAULT NULL,
  `add2` varchar(255) DEFAULT NULL,
  `add3` varchar(255) DEFAULT NULL,
  `add4` varchar(255) DEFAULT NULL,
  `add5` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `phone1` varchar(255) DEFAULT NULL,
  `map_lat` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `map_lng` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

LOCK TABLES `branch_address` WRITE;
/*!40000 ALTER TABLE `branch_address` DISABLE KEYS */;

INSERT INTO `branch_address` (`id`, `store_id`, `add1`, `add2`, `add3`, `add4`, `add5`, `postcode`, `phone1`, `map_lat`, `map_lng`, `date_created`, `date_modified`)
VALUES
  (1,331,'St John\'s Shopping Centre','Merrion St',NULL,'Leeds',NULL,'LS2 8LQ','01234567890',X'35332E3830303134',X'2D312E353433383037',NULL,NULL),
  (2,332,'White Rose Shopping Centre','Dewsbury Road',NULL,'Leeds',NULL,'LS11 8LU','01234567890',X'35332E373538333935',X'2D312E353734313438',NULL,NULL),
  (3,357,'120 Kirkgate','',NULL,'Leeds',NULL,'LS1 6BY','01234567890',X'35332E373937313038',X'2D312E3534303935',NULL,NULL),
  (6,481,'Trinity shopping centre','',NULL,'wakefield',NULL,'wf1 1qr','01234567890',X'35332E3638343932',X'2D312E3439363631',NULL,NULL),
  (7,252,'604/606 Little Horton Lane','',NULL,'Bradford',NULL,'BD5 0PD','01234567890',X'35332E37373933303033',X'2D312E37363633393032',NULL,NULL),
  (8,321,'Site E 19755','',NULL,'Bradford',NULL,'BD1 1TQ','01234567890',X'35332E373934373137',X'2D312E373535363136',NULL,NULL),
  (9,346,'6 Feasegate','',NULL,'York',NULL,'YO1 8SQ','01234567890',X'35332E393539313439',X'2D312E303832313933',NULL,NULL),
  (10,216,'2 - 6 Comercial Street','',NULL,'Halifax',NULL,'HX1 1TA','01234567890',X'35332E373232383035',X'2D312E383631353532',NULL,NULL),
  (11,223,'9 Northowram Green','',NULL,'Halifax',NULL,'HX3 7JE','01234567890',X'35332E37343133313437',X'2D312E383331323332',NULL,NULL);

/*!40000 ALTER TABLE `branch_address` ENABLE KEYS */;
UNLOCK TABLES;
